
-- SUMMARY --

This module provides a file field display type to allow MP3 files
displayed as Waveforms. Also there is a simple player included.

Web Audio Waveform Visualizer

http://www.wavesurfer.fm/

Customizable waveform audio visualization built on top of Web Audio API
and HTML5 Canvas. With wavesurfer.js you can assemble a full-featured
HTML5 audio player or create a sophisticated DJ application.

Compatibility

wavesurfer.js runs on modern browsers supporting Web Audio.
Including Firefox, Chrome, Safari, Mobile Safari and Opera.


-- REQUIREMENTS --

jQuery and file.


-- INSTALLATION --

1. Install the module and its dependencies
2. Download the wavesurfer library.
3. Place it in the libraries/wavesurfer folder
4. Make sure that wavesurfer.min.js is in the /wavesurfer folder
5. Add a file field and set Wavesurfer as the formatter
6. Enjoy!

-- CONFIGURATION --

  https://github.com/katspaugh/wavesurfer.js/blob/master/README.md

-- CUSTOMIZATION --

We have set up Surfer.field.js to allow customization.


-- TROUBLESHOOTING --


-- FAQ --


-- CONTACT --

Current maintainers
 Jarkko Oksanen (jOksanen) - https://www.drupal.org/user/2352292/
