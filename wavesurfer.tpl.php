<?php

/**
 * @file
 * Field.tpl.php Default template implementation to display the value of field.
 */
?>

<div id="waveform" data="<?php print $variables['url'] ?>">
  <div class="progress progress-striped active" id="progress-bar">
    <div class="progress-bar progress-bar-info"></div>
  </div>

  <!-- Here be the waveform -->
</div>

<div class="controls">
  <button class="btn btn-primary" data-action="skipback">
    <i class="glyphicon glyphicon-skip-backward"></i>
    <?php print t("Skip to Beginning") ?>
  </button>

  <button class="btn btn-primary" data-action="back">
    <i class="glyphicon glyphicon-step-backward"></i>
    <?php print t("Backward") ?>
  </button>

  <button class="btn btn-primary" data-action="play">
    <i class="glyphicon glyphicon-play"></i>
    <?php print t("Play
    /") ?>
    <i class="glyphicon glyphicon-pause"></i>
    <?php print t("Pause") ?>
  </button>

  <button class="btn btn-primary" data-action="forth">
    <i class="glyphicon glyphicon-step-forward"></i>
    <?php print t("Forward") ?>
  </button>

  <button class="btn btn-primary" data-action="skipforth">
    <i class="glyphicon glyphicon-skip-forward"></i>
    <?php print t("Skip to End") ?>
  </button>

  <span class="separator"></span>

  <button class="btn btn-primary" data-action="toggle-mute">
    <i class="glyphicon glyphicon-volume-off"></i>
    <?php print t("Toggle Mute") ?>
  </button>

  <div class="mark-controls">
      <button class="btn btn-flag-1" data-action="green-mark">
      <i class="glyphicon glyphicon-flag-1"></i>
        <?php print t("Set green mark") ?>
    </button>
  </div>
</div>
